<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('gallery/create', 'GalleryController@create')->name('gallery.create');
Route::post('gallery/store', 'GalleryController@store')->name('gallery.store');
Route::get('gallery/{gallery}/photos', 'GalleryController@photos')->name('gallery.photos');

Route::get('gallery/{gallery}/photos/create', 'PhotoController@create')->name('photos.create');
Route::post('gallery/{gallery}/photos/store', 'PhotoController@store')->name('photos.store');
Route::get('photos/{photo}/delete', 'PhotoController@delete')->name('photos.delete');