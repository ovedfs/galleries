<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function Gallery()
    {
    	return $this->belongsTo(Gallery::class);
    }
}
