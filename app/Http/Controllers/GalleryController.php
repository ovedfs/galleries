<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function create()
	{
		$user = auth()->user();

		return view('gallery.create', compact('user'));
	}

	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required|max:100',
			'description' => 'required|max:255'
		]);

		$gallery = new Gallery;
		$gallery->name = $request->name;
		$gallery->description = $request->description;
		$gallery->user_id = auth()->user()->id;

		if($gallery->save()){
			return redirect()->route('home')->with('msg', 'Galería creada correctamente!');
		}

		return redirect()->route('home')->with('msg', 'La Galería NO pudo ser creada!');
	}

	public function photos(Gallery $gallery)
	{
		return view('gallery.photos', compact('gallery'));
	}
}
