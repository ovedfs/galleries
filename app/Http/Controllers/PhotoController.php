<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Gallery;
use App\Photo;

class PhotoController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function create(Gallery $gallery)
	{
		return view('photo.create', compact('gallery'));
	}

	public function store(Request $request, Gallery $gallery)
	{
		$request->validate([
			'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048',
			'caption' => 'required|max:255'
		]);

		$file = $request->file('image');
		$filename = 'photo-' . time() . '.' . $file->getClientOriginalExtension();
		$file->storeAs('public', $filename);

		$photo = new Photo;
		$photo->image = $filename;
		$photo->caption = $request->caption;
		$photo->gallery_id = $gallery->id;

		if($photo->save()){
			return redirect()->route('gallery.photos', $gallery)->with('msg', 'Foto agregada correctamente!');
		}

		return redirect()->route('photos.create', $gallery)->with('msg', 'La Foto NO pudo ser agregada!');
	}

	public function delete(Photo $photo)
	{
		$gallery = $photo->gallery;
		Storage::delete('public/' . $photo->image);
		$deleted = $photo->delete();

		if($deleted){
			return redirect()->route('gallery.photos', $gallery)->with('msg', 'Foto eliminada correctamente!');
		}

		return redirect()->route('photos.create', $gallery)->with('msg', 'La Foto NO pudo ser eliminada!');
	}
}
