@include('partials.errors')

{{ Form::open([ 'route' => ['photos.store', $gallery], 'files' => true ]) }}
	
	<div class="form-group">
		<input type="file" name="image">
	</div>
	
	<div class="form-group">
		{{
			Form::text('caption', null, [
				'class' => 'form-control', 
				'placeholder' => '(Opcional) Ingresa una pequeña descripción para la foto...'
			])
		}}
	</div><hr>

	<div class="form-group">
		<a href="{{ route('gallery.photos', $gallery) }}" class="btn btn-secondary">
			<i class="fas fa-arrow-circle-left"></i> Regresar
		</a>
		<button type="submit" class="btn btn-primary">
			<i class="fas fa-save"></i> Guardar
		</button>
	</div>

{{ Form::close() }}