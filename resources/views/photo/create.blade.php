@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="text-center">
		<i class="fas fa-images"></i> {{ $gallery->name }}
		<small>[Agregar foto]</small>
	</h1>
	<h3 class="text-center">
		<span class="badge badge-secondary">{{ $gallery->description }}</span>
	</h3>
	<hr>

	<div class="row justify-content-center">
		<div class="col-md-6">
			
			<div class="card bg-light">
				<div class="card-header">
					<h4>Agregar foto</h4>
				</div>
				<div class="card-body">
					@include('photo.form-create')
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
