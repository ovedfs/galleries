<div class="card">
	<img class="card-img-top" src="{{ asset('storage/' . $photo->image) }}" alt="Card image cap">
	<div class="card-body">
		<h5 class="card-title">{{ $photo->caption }}</h5>
	</div>
	<div class="card-footer text-center">
		<a href="{{ route('photos.delete', $photo) }}" class="btn btn-danger">
			<i class="fas fa-trash-alt"></i>
		</a>
	</div>
</div>