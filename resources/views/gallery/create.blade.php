@extends('layouts.app')

@section('content')
<div class="container">
	<h1>
		<i class="fas fa-images"></i> MIS GALERÍAS
		<small>[Nueva Galería]</small>
	</h1><hr>

	<div class="row justify-content-center">
		<div class="col-md-6">
			
			<div class="card bg-light">
				<div class="card-header">
					<h4>Nueva Galería de {{ $user->name }}</h4>
				</div>
				<div class="card-body">
					@include('gallery.form-create')
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
