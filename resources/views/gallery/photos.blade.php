@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="text-center">
		<i class="fas fa-images"></i> {{ $gallery->name }}
		<a href="{{ route('photos.create', $gallery) }}" class="btn btn-primary">
			<i class="fas fa-plus-circle"></i> Foto
		</a>
	</h1>
	<h3 class="text-center">
		<span class="badge badge-secondary">{{ $gallery->description }}</span>
	</h3>
	<hr>

	@if($gallery->photos->count())
		<div class="card-columns">
			@foreach($gallery->photos as $photo)
				@include('photo.card')
			@endforeach	
		</div>
	@else
		<p class="alert alert-info">
			No has agregado fotos a esta Galería :(
		</p>
	@endif
</div>
@endsection
