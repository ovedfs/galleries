@include('partials.errors')

{{ Form::open([ 'route' => 'gallery.store' ]) }}
	
	<div class="form-group">
		{{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el nombre de la Galería...']) }}
	</div>
	
	<div class="form-group">
		{{
			Form::textarea('description', null, [
				'class' => 'form-control', 
				'placeholder' => 'Ingresa una descripción para la Galería...',
				'rows' => 3
			])
		}}
	</div><hr>

	<div class="form-group">
		<a href="{{ route('home') }}" class="btn btn-secondary">
			<i class="fas fa-arrow-circle-left"></i> Regresar
		</a>
		<button type="submit" class="btn btn-primary">
			<i class="fas fa-save"></i> Guardar
		</button>
	</div>

{{ Form::close() }}