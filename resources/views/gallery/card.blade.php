<div class="card">
	<div class="card-header">
		<h5>{{ $gallery->name }}</h5>
	</div>
	<div class="card-body">
		<p>{{ $gallery->description }}</p>
	</div>
	<div class="card-footer text-center">
		<a href="{{ route('gallery.photos', $gallery) }}" class="btn btn-secondary">
			Ver fotos
		</a>
	</div>
</div>