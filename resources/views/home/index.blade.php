@extends('layouts.app')

@section('content')
<div class="container">
	<h1>
		<i class="fas fa-images"></i> MIS GALERÍAS
		<a href="{{ route('gallery.create') }}" class="btn btn-primary">
			<i class="fas fa-plus-circle"></i> Galería
		</a>
	</h1><hr>

	@if($user->galleries->count())
		<p>Hola {{ $user->name }}, has creado {{ $user->galleries->count() }} Galería(s):</p>

		<div class="card-columns">
			@foreach($user->galleries as $gallery)
				@include('gallery.card')
			@endforeach
		</div>
	@else
		<p class="alert alert-info">
			Aún no has creado ninguna Galería :(
		</p>
	@endif
</div>
@endsection
